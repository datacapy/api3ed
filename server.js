// ------ Server

var express = require('express')
var bodyParser = require('body-parser')
var app = express()

var requestJson = require('request-json')

app.use(bodyParser.json())

// s3 d1 conexion datos a mongodb en mLab

// recuperar info de todos los usuarios

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/dbbanco_ccf/collections"
var apiKey = "apiKey=up4WJk7bSjzAhl-D4iNYkZ_E3WAmcRGq"
var clienteMlab

app.get('/apitechu/v5/usuarios', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      res.send(body)
    }
  })
})

// probar en postman con:
// GET http://localhost:3000/apitechu/v5/usuarios

// recuperar info de un usuario concreto

app.get('/apitechu/v5/usuarios/:id', function(req, res) {
  var id = req.params.id
  var query = 'q={"id":' + id  + '}'
  //clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0) {
        var datos = {}
        datos.nombre = body[0].nombre
        datos.apellidos = body[0].apellido
        res.send(datos)
        // res.send(body[0])
      }
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})

// Inicio de sesión de un usuario: verificar credenciales de un login

app.get('/apitechu/v5/login/:email/:password', function(req, res) {
  // Angel lo ha implementado pasando estas variables en el header
  var email = req.headers.email
  var password = req.headers.password
  // Alberto lo ha hecho pasandolos como parametros
  var email = req.params.email
  var password = req.params.password
  var query = 'q={"email":"' + email + '", "password":"' + password + '"}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&" + apiKey)

  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0) {
        // añadir logged=true en el registro del usuario logeado
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
        var cambio = '{"$set":{"logged":true}}'
        clienteMlab.put('?q={"id":' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
          var datos = {}
          datos.login = "ok"
          datos.id = body[0].id
          datos.nombre = body[0].nombre
          datos.apellidos = body[0].apellido
          res.send(datos)
        })
      }
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})

// Logout de usuario (via post con parametros en body)

app.post('/apitechu/v5/logout', function(req, res) {
  // Angel lo ha implementado pasando estas variables en el header
  var body = req.body
  console.log(body)
  var id = body.id
  console.log("id = ["+id+"]")
  //var query = 'q={"id":' + id + '}'
  var query = 'q={"id":' + id + ', "logged":true}'
  console.log(query)
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&" + apiKey)

  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0) {
        // añadir logged=false en el registro del usuario logeado
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
        var cambio = '{"$set":{"logged":false}}'
        clienteMlab.put('?q={"id":' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
          var datos = {}
          datos.logout = "ok"
          datos.id = id
          res.send(datos)
        })
      }
      else {
        res.status(404).send('Usuario no logado en el sistema')
      }
    }
  })
})

// Dado un numero de cuenta, devolver los movimientos, extraidos de base de datos

app.get('/apitechu/v5/movimientos/:iban', function(req, res) {
  var iban = req.params.iban

  var query = 'q={"iban":"' + iban + '"}'
  var filter = 'f={"movimientos":1,"_id":0}'

  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + filter + "&" + apiKey)

  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0) {
        // hay cuentas
          res.send(body)
      }
      else {
        res.status(404).send('No existe ningun cliente con este IBAN')
      }
    }
  })
})

// Dado un cliente, devolver todas sus cuentas, extraidas de base de datos

app.get('/apitechu/v5/cuentas/:idcliente', function(req, res) {
  var idCliente = req.params.idcliente
  console.log("idcliente="+idCliente)

  var query = 'q={"idcliente":' + idCliente + '}'
  var filter = 'f={"iban":1,"_id":0}'

  console.log(query)
  console.log(filter)

  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + filter + "&" + apiKey)

  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0) {
        // hay cuentas
          res.send(body)
      }
      else {
        res.status(404).send('No existe este cliente o no tiene cuentas')
      }
    }
  })
})



// -----------------------------------------------------------------

var port = process.env.PORT || 3000
var fs = require('fs')

app.listen(port)

console.log("Hola, amado lider")
console.log("API escucha tus deseos en el puerto " + port)

// ------------------- API de usuarios

//var usuarios = require('./usuarios.json')
var usuarios = require('./users.json')
var users = require('./users.json')
var cuentas = require('./cuentas.json')

// Devolver mensaje estático

app.get('/apitechu/v1', function(req, res) {
  //console.log(req)
  res.send({"message":"Bienvenido a mi API"})
})

// Devolver lista de usuarios

app.get('/apitechu/v1/usuarios', function(req, res) {
  res.send(usuarios)
})

// Dar de alta nuevo usuario

app.post('/apitechu/v1/usuarios', function(req, res) {
  var nuevo = {"first_name":req.headers.first_name,
              "country": req.headers.country}
  usuarios.push(nuevo)
  console.log(nuevo)
  console.log(req.headers)
  const datos = JSON.stringify(usuarios)
  // escribimos datos actualizados a fichero
  fs.writeFile("./usuarios.json", datos, "utf-8", function(err) {
    if(err) {
      console.log(err)
    }
    else {
      console.log("fichero guardado")
    }
  })
  res.send("Alta OK")
})

// Borrar usuario de la lista de usuarios

app.delete('/apitechu/v1/usuarios/:id', function(req, res) {
  usuarios.splice(req.params.id-1, 1)
  res.send("usuario borrado")
})

// Devolver lista de usuarios

app.post('/apitechu/v1/monstruo/:p1/:p2', function(req, res) {
  console.log("Parametros")
  console.log(req.params)
  console.log("Query strings")
  console.log(req.query)
  console.log("Headers")
  console.log(req.headers)
  console.log("Body")
  console.log(req.body)
  //res.send("ok")
  var nuevo = req.body
  usuarios.push(nuevo)
  console.log(nuevo)
  const datos = JSON.stringify(usuarios)
  // escribimos datos actualizados a fichero
  fs.writeFile("./usuarios.json", datos, "utf-8", function(err) {
    if(err) {
      console.log(err)
    }
    else {
      console.log("fichero guardado")
    }
  })
  res.send("Alta OK v2")
})

// Dar de alta nuevo usuario - v2 - leemos datos en body en lugar de url

app.post('/apitechu/v2/usuarios/:p1/:p2', function(req, res) {
  console.log("Parametros")
  console.log(req.params)
  console.log("Query strings")
  console.log(req.query)
  console.log("Headers")
  console.log(req.headers)
  console.log("Body")
  console.log(req.body)
  //res.send("ok")
  var nuevo = req.body
  usuarios.push(nuevo)
  console.log(nuevo)
  const datos = JSON.stringify(usuarios)
  // escribimos datos actualizados a fichero
  fs.writeFile("./usuarios.json", datos, "utf-8", function(err) {
    if(err) {
      console.log(err)
    }
    else {
      console.log("fichero guardado")
    }
  })
  res.send("Alta OK v2")
})

// login

app.post('/apitechu/v1/login', function(req, res) {
  console.log("--- login --")
  console.log("Headers")
  console.log(req.headers)
  var email = req.headers.email
  var pwd = req.headers.password
  var id = 0

  for (var i = 0; i < users.length; i++) {
    if (users[i].email == email && users[i].password == pwd) {
      id = users[i].id
      users[i].logged = true
      console.log(users[i])
      break
    }
  }
  if (id != 0) {
    res.send({ "encontrado":"si", "id":id })
  } else {
    res.send({ "encontrado":"no"})
  }
})

// logout

app.post('/apitechu/v1/logout', function(req, res) {
  console.log("--- logout --")
  console.log("Headers")
  console.log(req.headers)
  var id = req.headers.id
  var found = false
  var logged = false

  for (var i = 0; i < users.length; i++) {
    if (users[i].id == id) {
      if (users[i].logged) {
        console.log(users[i])
        users[i].logged = false
        console.log(users[i])
        logged = true
      }
      found = true
      break
    }
  }
  if (found && logged) {
    res.send({ "logout":id })
  } else {
    res.send("user is not logged")
  }
})

// API de cuentas

// Devolver lista de cuentas

app.get('/apitechu/v1/cuentas', function(req, res) {

  var listadoCuentas = {}
  var iban = []

  for (var i = 0; i < cuentas.length; i++) {
    // var cuenta = {}
    // cuenta.iban = cuentas[i].iban
    // iban.push(cuenta)
    iban.push(cuentas[i].iban)
  }

  listadoCuentas.iban = iban
  res.send(listadoCuentas)
})

// Devolver cuentas de un usuario

app.get('/apitechu/v1/cuentas/:idcliente', function(req, res) {

  var listadoCuentas = {}
  var iban = []
  var id = req.params.idcliente

  for (var i = 0; i < cuentas.length; i++) {
    if (cuentas[i].idcliente == id) {
      iban.push(cuentas[i].iban)
    }
  }

  listadoCuentas.idcliente = id
  listadoCuentas.iban = iban

  res.send(listadoCuentas)
})

// Devolver movimiento de una cuenta

app.get('/apitechu/v1/movimientos/:iban', function(req, res) {

  var listadoMovs = {}
  var movs = []
  var iban = req.params.iban

  for (var i = 0; i < cuentas.length; i++) {
    if (cuentas[i].iban == iban)
      movs.push(cuentas[i].movimientos)
  }

  listadoMovs.iban = iban
  listadoMovs.movimientos = movs

  res.send(listadoMovs)
})
