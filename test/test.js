var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')
var server = require('../server')
var chould = chai.should()

chai.use(chaiHttp) // configurar chai con modulo http

// Definimos el conjunto de pruebas
// Utilizamos funciones lambda (ie punteros a funciones)

describe('Tests de conectividad', () => {
  it('Google funciona', (done) => {
    // prueba 1: probar que google funciona utilizando un callback
    chai.request('http://www.google.es')
    .get('/')
    .end((err, res) => {
      // console.log(res)
      res.should.have.status(200)
      done()
    })
  })
})

describe ('Tests de API usuarios', () => {
  it('Raiz OK', (done) => {
    // prueba 2: probar que mi API funciona
    chai.request('http://localhost:3000')
    .get('/apitechu/v1')
    .end((err, res) => {
      //console.log(res)
      res.should.have.status(200)
      res.body.message.should.be.eql("Bienvenido a mi API")
      done()
    })
  })

  it('Lista de usuarios', (done) => {
    // prueba 2: probar que la respuesta es un array
    chai.request('http://localhost:3000')
    .get('/apitechu/v1/usuarios')
    .end((err, res) => {
      //console.log(res)
      res.should.have.status(200)
      res.body.should.be.a('array')
      for (var i = 0; i < res.body.length; i++) {
        res.body[i].should.have.property('email')
        res.body[i].should.have.property('password')
      }
      done()
    })
  })
})
